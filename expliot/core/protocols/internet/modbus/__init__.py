"""Wrapper for the Modbus integration."""

from pymodbus.client import ModbusTcpClient as MBTClient


class ModbusTcpClient(MBTClient):
    """Wrapper for the Modbus client."""
