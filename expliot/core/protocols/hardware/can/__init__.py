"""Wrapper for CANbus communication."""

from can import Message
from can.interface import Bus


class CanBus(Bus):
    """A simple wrapper around python-can Bus class."""



class CanMessage(Message):
    """A simple wrapper around python-can Message class."""
